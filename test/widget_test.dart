// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:prof2024/domain/utils.dart';

import 'package:prof2024/main.dart';
import 'package:prof2024/presentation/pages/log_in.dart';
import 'package:prof2024/presentation/theme/theme.dart';

void main() {
  group('tests', () {
    test('valid-email-test', (){
      var notValidEmail = 'BAd-EmaIl@.com';
      bool val = checkEmail(notValidEmail);
      expect(val, false);
      var validEmail = 'validemail@gmail.com';
      bool val1 = checkEmail(validEmail);
      expect(val1, true);
    });
    test('valid-password-test', (){
      var notValidPassword = '';
      bool val = checkPassword(notValidPassword);
      expect(val, false);
      var validPassword = '12345678';
      bool val1 = checkPassword(validPassword);
      expect(val1, true);
    });

    testWidgets('show-error-on-not-valid-email ', (widgetTester)async{
      await widgetTester.pumpWidget(ScreenUtilInit(
        designSize: Size(375, 812),
        builder: (_, __){
          return MaterialApp(
            title: 'Flutter Demo',
            theme: theme,
            home: LogIn(),
          );
        },
      ));
      await widgetTester.pumpAndSettle();
      await widgetTester.enterText(find.byKey(Key('Email-Field')), 'NotValidEmail');
      await widgetTester.enterText(find.byKey(Key('Password-Field')), '12345678');
      await widgetTester.tap(find.byKey(Key('Button')), warnIfMissed: false);
      await widgetTester.pumpAndSettle();
      expect(find.byKey(Key('AlertError')), findsOneWidget);
    });

    testWidgets('show-error-on-not-valid-password ', (widgetTester)async{
      await widgetTester.pumpWidget(ScreenUtilInit(
        designSize: Size(375, 812),
        builder: (_, __){
          return MaterialApp(
            home: LogIn(),
          );
        },
      ));
      await widgetTester.pumpAndSettle();
      await widgetTester.enterText(find.byKey(Key('Email-Field')), 'validemail@gmail.com');
      await widgetTester.enterText(find.byKey(Key('Password-Field')), '');
      await widgetTester.tap(find.byKey(Key('Button')),warnIfMissed: false);
      await widgetTester.pumpAndSettle();
      expect(find.byKey(Key('AlertError')), findsOneWidget);
    });
  });
}
