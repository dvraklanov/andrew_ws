import 'package:flutter/material.dart';
import 'package:prof2024/domain/otp_presenter.dart';
import 'package:prof2024/domain/profile_tab_presenter.dart';
import 'package:prof2024/presentation/pages/home.dart';

void showError(BuildContext context, String e){
  showDialog(context: context, builder: (_) => AlertDialog(
    key: Key('AlertError'),
    title: Text('Error'),
    content: Text(e),
    actions: [
      TextButton(
          onPressed: (){
            Navigator.of(context).pop();
          },
          child: Text('OK')
      )
    ],
  ));
}

void showLoading(BuildContext context){
  showDialog(context: context, builder: (_) => Dialog(
    backgroundColor: Colors.transparent,
    surfaceTintColor: Colors.transparent,
    child: Center(
      child: CircularProgressIndicator(),
    ),
  ));
}

void showGeneratePassword(BuildContext context){
  TextEditingController controller = TextEditingController();
  showDialog(context: context, builder: (_) => AlertDialog(
    title: Text('Input password'),
    content: TextField(
      controller: controller,
    ),
    actions: [
      TextButton(
        onPressed: (){
          Navigator.of(context).pop();
          showNewPassword(context, controller.text);
        },
        child: Text('OK')
    )],
  ));
}

void showNewPassword(BuildContext context, String password){
  showDialog(context: context, builder: (_) => AlertDialog(

    title: Text('Your new password'),
    content: SelectableText(createPassword(password)),
    actions: [
      TextButton(
          onPressed: (){
            pressUpdatePassword(
                password,
                (){
                  Navigator.of(context).pop();
                  Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => Home()), (route) => false);
                },
                    (p0) => null
            );
          },
          child: Text('OK')
      )],
  ));
}

void showPickAvatar(BuildContext context, Function onResponse){
  showDialog(context: context, builder: (_) => AlertDialog(
    title: Text('Выберите ресурс'),
    actions: [
      TextButton(onPressed: (){
        pickAvatarCamera((res){
          Navigator.of(context).pop();
          onResponse(res);
        });
      }, child: Text('camera')),
      TextButton(onPressed: (){
        pickAvatarGallery((res){
          Navigator.of(context).pop();
          onResponse(res);
        });
      }, child: Text('gallery')),
      TextButton(onPressed: (){}, child: Text('kondinsky'))
    ],
  ));
}