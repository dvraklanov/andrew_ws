import 'package:change_case/change_case.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:prof2024/domain/forgot_password_presenter.dart';
import 'package:prof2024/domain/log_in_presenter.dart';
import 'package:prof2024/domain/utils.dart';
import 'package:prof2024/presentation/pages/home.dart';
import 'package:prof2024/presentation/pages/otp_page.dart';
import 'package:prof2024/presentation/pages/sign_up.dart';
import 'package:prof2024/presentation/theme/colors.dart';
import 'package:prof2024/presentation/widgets/custom_text_field.dart';
import 'package:prof2024/presentation/widgets/dialogs.dart';

class ForgotPassword extends StatefulWidget {
  const ForgotPassword({super.key});

  @override
  State<ForgotPassword> createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  TextEditingController email = TextEditingController();
  bool enableButton = true;
  void onChanged(_){}
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      backgroundColor: colors.block,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(top: 66.w, left: 20.w, right: 20.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: (){
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => const Home()));
                },
                child: Container(
                  height: 44.w,
                  width: 44.w,
                  decoration: BoxDecoration(
                      color: colors.background,
                      borderRadius: BorderRadius.circular(360)
                  ),
                  child: SvgPicture.asset('assets/back.svg', color: colors.text, fit: BoxFit.scaleDown,),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Забыл Пароль',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                  SizedBox(height: 8.w),
                  Text(
                    'Введите Свою Учетную Запись\nДля Сброса',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                  SizedBox(height: 40.w),
                  CustomTextField(
                      key: const Key('Email-Field'),
                      label: 'Email',
                      hint: 'xyz@gmail.com',
                      controller: email,
                      onChanged: onChanged
                  ),
                  SizedBox(height: 40.w),
                  SizedBox(
                    height: 50.w,
                    width: double.infinity,
                    child: FilledButton(
                        key: const Key('Button'),
                        onPressed: (enableButton) ? (){
                          pressSendOTP(
                              email.text,
                                  (){
                                Navigator.of(context).push(MaterialPageRoute(builder: (context) => OTPPage(email: email.text)));
                              },
                                  (String e){showError(context, e);}
                          );
                        } : null,
                        style: Theme.of(context).filledButtonTheme.style,
                        child: const Text('Отправить')
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}