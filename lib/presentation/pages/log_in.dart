import 'package:change_case/change_case.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:prof2024/domain/log_in_presenter.dart';
import 'package:prof2024/domain/utils.dart';
import 'package:prof2024/presentation/pages/forgot_password.dart';
import 'package:prof2024/presentation/pages/home.dart';
import 'package:prof2024/presentation/pages/sign_up.dart';
import 'package:prof2024/presentation/theme/colors.dart';
import 'package:prof2024/presentation/widgets/custom_text_field.dart';
import 'package:prof2024/presentation/widgets/dialogs.dart';

class LogIn extends StatefulWidget {
  const LogIn({super.key});

  @override
  State<LogIn> createState() => _LogInState();
}

class _LogInState extends State<LogIn> {
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  bool enableButton = true;
  void onChanged(_){}
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      backgroundColor: colors.block,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(top: 66.w, left: 20.w, right: 20.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: (){
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => const Home()));
                },
                child: Container(
                  height: 44.w,
                  width: 44.w,
                  decoration: BoxDecoration(
                      color: colors.background,
                      borderRadius: BorderRadius.circular(360)
                  ),
                  child: SvgPicture.asset('assets/back.svg', color: colors.text, fit: BoxFit.scaleDown,),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Привет!',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                  SizedBox(height: 8.w),
                  Text(
                    'Заполните Свои Данные Или\n Продолжите Через Социальные Медиа',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                  SizedBox(height: 30.w),
                  CustomTextField(
                    key: const Key('Email-Field'),
                      label: 'Email',
                      hint: 'xyz@gmail.com',
                      controller: email,
                      onChanged: onChanged
                  ),
                  SizedBox(height: 30.w),
                  CustomTextField(
                      key: const Key('Password-Field'),
                      label: 'Пароль',
                      hint: '••••••••',
                      controller: password,
                      enableObscure: true,
                      onChanged: onChanged
                  ),
                  SizedBox(height: 12.w),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      GestureDetector(
                        onTap: (){
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => const ForgotPassword()));
                        },
                        child: Text(
                          'Востановить',
                          style: Theme.of(context).textTheme.labelLarge,
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 24.w),
                  SizedBox(
                    height: 50.w,
                    width: double.infinity,
                    child: FilledButton(
                        key: const Key('Button'),
                        onPressed: (enableButton) ? (){
                          pressSignIn(
                              email.text,
                              password.text,
                              (){
                                Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => const Home()), (route) => false);
                              },
                              (String e){showError(context, e);}
                          );
                        } : null,
                        style: Theme.of(context).filledButtonTheme.style,
                        child: const Text('Войти')
                    ),
                  ),
                  SizedBox(height: 209.w),
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => const SignUp()));
                    },
                    child: RichText(text: TextSpan(
                      children: [
                        TextSpan(
                          text: 'Вы впервые? ',
                          style: Theme.of(context).textTheme.labelMedium?.copyWith(color: colors.hint)
                        ),
                        TextSpan(
                          text: 'Создать пользователя',
                          style: Theme.of(context).textTheme.labelMedium
                        )
                      ]
                    )),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}