import 'package:flutter/material.dart';

abstract class ColorsApp{
  abstract final Color background;
  abstract final Color disable;
  abstract final Color accent;
  abstract final Color subTextLight;
  abstract final Color subTextDark;
  abstract final Color block;
  abstract final Color text;
  abstract final Color hint;
  abstract final Color red;
  abstract final Color cont;
  abstract final Color cont1;
}

class LightColorsApp extends ColorsApp{
  @override
  // TODO: implement accent
  Color get accent => Color.fromARGB(255, 72, 178, 231);

  @override
  // TODO: implement background
  Color get background => Color.fromARGB(255, 247, 247, 249);

  @override
  // TODO: implement block
  Color get block => Color.fromARGB(255, 255, 255, 255);

  @override
  // TODO: implement disable
  Color get disable => Color.fromARGB(255, 43, 107, 139);

  @override
  // TODO: implement hint
  Color get hint => Color.fromARGB(255, 106, 106, 106);

  @override
  // TODO: implement red
  Color get red => Color.fromARGB(255, 248, 114, 101);

  @override
  // TODO: implement subTextDark
  Color get subTextDark => Color.fromARGB(255, 112, 123, 129);

  @override
  // TODO: implement subTextLight
  Color get subTextLight => Color.fromARGB(255, 216, 216, 216);

  @override
  // TODO: implement text
  Color get text => Color.fromARGB(255, 43, 43, 43);

  @override
  // TODO: implement cont
  Color get cont => Color.fromARGB(102, 217, 217, 217);

  @override
  // TODO: implement cont1
  Color get cont1 => Color.fromARGB(59, 247, 247, 249);
}