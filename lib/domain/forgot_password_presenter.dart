import 'package:prof2024/data/repository/supabase.dart';
import 'package:prof2024/domain/utils.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> pressSendOTP(String email, Function onResponse, Function(String) onError) async {
  try{
    if(!checkEmail(email)){
      onError('Email not valid');
      return;
    }
    await sendOTP(email);
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.message);
  }on Exception catch(e){
    onError(e.toString());
  }
}