bool checkEmail(String email){
  return RegExp(r'^[0-9a-z]+@[0-9a-z]+\.\w{3,}$').hasMatch(email);
}

bool checkPassword(String password){
  return password.isNotEmpty;
}

void showingError(String email, String password, Function(String) onError){
  if(!checkEmail(email)){
    onError('Email not valid');
    return;
  }
  if(!checkPassword(password)){
    onError('Input Password');
    return;
  }
}