import 'dart:typed_data';

import 'package:image_picker/image_picker.dart';
import 'package:prof2024/data/repository/supabase.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> pickAvatarCamera(Function onPick) async {
  XFile? avatarFile = await ImagePicker().pickImage(source: ImageSource.camera);
  if(avatarFile != null){
    var avatar = await avatarFile.readAsBytes();
    onPick(avatar);
  }
}

Future<void> pickAvatarGallery(Function onPick) async {
  XFile? avatarFile = await ImagePicker().pickImage(source: ImageSource.gallery);
  if(avatarFile != null){
    var avatar = await avatarFile.readAsBytes();
    onPick(avatar);
  }
}

Map<String, dynamic>? getData(){
  return getUserMetaData();
}

Future<void> pressUpdate(String name, String secondName, String address, String phone, Function onResponse, Function(String) onError) async {
  try{
    await updateUserData(name, secondName, phone, address);
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.message);
  }on Exception catch(e){
    onError(e.toString());
  }
}

Future<void> pressUpdateAvatar(Uint8List avatar, Function onResponse, Function(String) onError) async {
  try{
    await updateAvatar(avatar);
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }on PostgrestException catch(e){
    onError(e.message);
  }on Exception catch(e){
    onError(e.toString());
  }
}

