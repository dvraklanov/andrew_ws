import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prof2024/presentation/pages/home.dart';
import 'package:prof2024/presentation/pages/log_in.dart';
import 'package:prof2024/presentation/theme/theme.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> main() async {
  await Supabase.initialize(
    url: 'https://zyelpagqoxshfqonovof.supabase.co',
    anonKey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Inp5ZWxwYWdxb3hzaGZxb25vdm9mIiwicm9sZSI6ImFub24iLCJpYXQiOjE2ODg0NTM3MzAsImV4cCI6MjAwNDAyOTczMH0.9ay0Dim2QmXPdhIkc91NkxJLRYh91_mE2ruF-KOuj8U',
  );

  runApp(MyApp());
}


class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(375, 812),
      builder: (_, __){
        return MaterialApp(
          title: 'Flutter Demo',
          theme: theme,
          home: const Home(),
        );
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold();
  }
}
